#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re

"""Baby Names exercise

Define the extract_names() function below and change main()
to call it.

For writing regex, it's nice to include a copy of the target
text for inspiration.

Here's what the html looks like in the baby.html files:
...
<h3 align="center">Popularity in 1990</h3>
....
<tr align="right"><td>1</td><td>Michael</td><td>Jessica</td>
<tr align="right"><td>2</td><td>Christopher</td><td>Ashley</td>
<tr align="right"><td>3</td><td>Matthew</td><td>Brittany</td>
...

Suggested milestones for incremental development:
 -Extract the year and print it
 -Extract the names and rank numbers and just print them
 -Get the names data into a dict and print it
 -Build the [year, 'name rank', ... ] list and print it
 -Fix main() to use the extract_names list
"""

def extract_names(filename):
  """
  Given a file name for baby.html, returns a list starting with the year string
  followed by the name-rank strings in alphabetical order.
  ['2006', 'Aaliyah 91', Aaron 57', 'Abagail 895', ' ...]
  """
  # +++your code here+++
  # var dict it's call names
  names = []
  # open the file an read and keep in var f
  with open(filename, "rU") as f:
    # in text we read the text of filename
    text = f.read()
    # use regular expresion to search years in the text
    match_year = re.search(r'Popularity\sin\s(\d\d\d\d)', text)
  if not match_year:
    # We didn't find a year, so we'll exit with an error message.
    sys.stderr.write('Couldn\'t find the year!\n')
    sys.exit(1)
  year = match_year.group(1)
  names.append(year)
  #-------------------------number--------name.boy------name.girl-----
  tuples = re.findall(r'<td>(\d+)</td><td>(\w+)</td><td>(\w+)</td>', text)
  names_to_rank = {}
  for rank_tuple in tuples:
    (rank, boyname, girlname) = rank_tuple  # unpack the tuple into 3 vars
    if boyname not in names_to_rank:
      names_to_rank[boyname] = rank
    if girlname not in names_to_rank:
      names_to_rank[girlname] = rank

  sorted_names = sorted(names_to_rank.keys())
  # Build up result list, one element per line
  for name in sorted_names:
    names.append(name + " " + names_to_rank[name])
  return names
  #return match

def main():
  # This command-line parsing code is provided.
  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  # creo una variable on guardo els arguments per pantalla del terminal
  args = sys.argv[1:]
  # si no te cap argument, imprimeix per pantalla el seguent missatge
  if not args:
    print('usage: [--summaryfile] file [file ...]')
    sys.exit(1) # surt del programa
  # Notice the summary flag and remove it from args if it is present.
  # nova variable que es diu summary i es false
  summary = False
  # Si el argument tal es igual a la cadena seguent, summary es true i
  # eliminar el argument
  if args[0] == '--summaryfile':
    summary = True
    del args[0]

  # +++your code here+++
  # Aquest bucle serveix per crida la funcio extraure noms del filtre
  for filename in args:
    final_list = extract_names(filename)
    text = '\n'.join(final_list) + '\n'
    match_text = re.search(r'\d\d\d\d', text)
    year = match_text.group(0)
    print(year)
    #print(text)

    with open('%s.summary' % filename, "w", encoding = "utf-8") as new_file:
      new_file.write(text)

if __name__ == '__main__':
  main()
