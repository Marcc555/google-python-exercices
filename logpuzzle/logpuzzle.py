#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import os
import re
import sys
import urllib
from urllib.request import urlretrieve
from urllib.request import urlopen

"""Logpuzzle exercise
Given an apache logfile, find the puzzle urls and download the images.

Here's what a puzzle url looks like:
10.254.254.28 - - [06/Aug/2007:00:13:48 -0700] "GET /~foo/puzzle-bar-aaab.jpg HTTP/1.0" 302 528 "-" "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6"
"""

def sort_urls(url):
  match = re.search(r'-(\w+)-(\w+)\.\w+', url)
  if match:
    return match.group(2)
    print(match)
  else:
    return url


def read_urls(filename):
  """Returns a list of the puzzle urls from the given log file,
  extracting the hostname from the filename itself.
  Screens out duplicate urls and returns the urls sorted into
  increasing order."""
  # +++your code here+++
  url_dict = {}
  underbar = filename.index('_')
  host = filename[underbar + 1:]
  with open(filename, 'rU') as f:
    for line in f:
      match_text = re.search(r'"GET (\S+)', line)
      if match_text:
        path = match_text.group(1)
        if 'puzzle' in path:
          url_dict['http://' + host + path] = 1

  return sorted(url_dict.keys(), key=sort_urls)



def download_images(img_urls, dest_dir):
  """Given the urls already in the correct order, downloads
  each image into the given directory.
  Gives the images local filenames img0, img1, and so on.
  Creates an index.html in the directory
  with an img tag to show each local image file.
  Creates the directory if necessary.
  """
  # +++your code here+++
  #os.mkdir(dest_dir)  # create a directory

  body = ''
  for n, url in enumerate(img_urls, 1):
    filename = 'img'+ str(n)
    file_path = dest_dir
    path = 'file:///home/vant/Documentos/Google_exercicies/google-python-exercises/logpuzzle/'
    if not os.path.exists(file_path):
      print(file_path, '???')
      os.makedirs(file_path)
    urlretrieve(url, filename + '.jpg')
    print('Retrieving...', url)
    body =  body + '<img src="%s%s.jpg">' % ((path),(filename))
  
  h = open('index.html', 'w')
  send = """<verbatim>
  <html>
  <body>""" + body + """</body>
  </html>"""
  h.write(send)
  h.close()
  #for n, url in enumerate(img_urls, 1):
  #  print(n, url)
    #ufile = urlopen(url)
  #  with open('img'+ str(n), "w", encoding = "utf-8") as new_file:
  #    new_file.write(urlretrieve(url, new_file))
  #urllib.urlretrieve(url, filename)


def main():
  args = sys.argv[1:]

  if not args:
    print('usage: [--todir dir] logfile ')
    sys.exit(1)

  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  img_urls = read_urls(args[0])
  print('\n'.join(img_urls))

  if todir:
    download_images(img_urls, todir)

if __name__ == '__main__':
  main()
